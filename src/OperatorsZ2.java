import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class OperatorsZ2 {
    public void run () throws IOException {
        System.out.println("Найти Если а – четное посчитать а*б, иначе а+б");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите первое число");
        int chisloOne = Integer.parseInt(reader.readLine());
        System.out.println("Введите второе число");
        int chisloTwo = Integer.parseInt(reader.readLine());

        if (chisloOne%2 == 0f)
            System.out.println(chisloOne*chisloTwo);
        else
            System.out.println(chisloOne+chisloTwo);
    }
}
