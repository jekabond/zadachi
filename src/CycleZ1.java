public class CycleZ1 {
    public void run() {
        System.out.println("Найти сумму четных чисел и их количество в диапазоне от 1 до 99");
        int b = 0, c = 0;
        for (int x = 1; x < 99; x+=2){
            if (x % 2 != 0) {
                b++;
                c = c + x;
            }
        }
        System.out.println("amount of numbers = " + b);
        System.out.println("numbers sum = " + c);
    }

}

