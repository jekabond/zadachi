import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class CycleZ2 {
    public void run() throws IOException {
        System.out.println("Проверить простое ли число");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите число");
        int number = Integer.parseInt(reader.readLine());

        for ( int i=2; i < number; i++) {
            if ( number%i == 0) {
                System.out.println("Число составное");
                return;
            }
        }
        System.out.println("Число простое");
    }
}

