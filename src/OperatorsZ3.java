import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class OperatorsZ3 {
    public void run() throws IOException {
        System.out.println("Cуммы только положительных из трех чисел");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите первое число");
        int chisloOne = Integer.parseInt(reader.readLine());

        System.out.println("Введите второе число");
        int chisloTwo = Integer.parseInt(reader.readLine());

        System.out.println("Введите третье число");
        int chisloThree = Integer.parseInt(reader.readLine());

        int a, b, c;

        if (chisloOne > 0)
            a = chisloOne;
        else
            a = 0;
        if (chisloTwo > 0)
            b = chisloTwo;
        else
            b = 0;
        if (chisloThree > 0)
            c = chisloThree;
        else
            c = 0;

        int operSum= a+b+c;
        System.out.println("Сумма ваших положительных чисел = " +operSum);

    }
}
