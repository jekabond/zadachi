import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class CycleZ4 {

    public void  run() throws IOException {
        System.out.println("Вычислить факториал числа n");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Ввидите число");
        int n = Integer.parseInt(reader.readLine());
        System.out.println(opredelenieFactoriala(n));

    }
    public static int opredelenieFactoriala (int n){
        int result = 1;
        for (int a = 1; a<=n; a++){
            result = result*a;
        }
        return result;
    }

}
