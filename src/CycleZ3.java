import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class CycleZ3 {
    public void run() throws IOException {
        System.out.println("Найти корень натурального числа с точностью до целого");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите число");
        int chislo = Integer.parseInt(reader.readLine());
        System.out.println(podborKornya(++chislo));
        }

    public static int podborKornya (int chislo) {
        int result = 1;
        for (int i = 1; (i*i) < chislo; i++) {
            result = result*0+i;
        }
        return result;
    }

}
