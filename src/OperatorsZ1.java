import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class OperatorsZ1 {


    public void  run() throws IOException {
        System.out.println("Определить какой четверти принадлежит точка с координатами (х,у)");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите первое число");
        int chisloOne = Integer.parseInt(reader.readLine());
        System.out.println("Введите второе число");
        int chisloTwo = Integer.parseInt(reader.readLine());

        if (chisloOne > 0 && chisloTwo > 0)
            System.out.println("Число в первой четверти");
        if (chisloOne > 0 && chisloTwo < 0)
            System.out.println("Число во второй четверти");
        if (chisloOne < 0 && chisloTwo < 0)
            System.out.println("Число в третьей четверти");
        if (chisloOne < 0 && chisloTwo > 0)
            System.out.println("Число в четвертой четверти");
    }
}
