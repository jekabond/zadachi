import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class CycleZ6 {
    public void run() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите число");
        long chislo = Long.parseLong(reader.readLine());
        long mirror = 0l;

        while(chislo != 0){
            mirror = (chislo % 10);
            chislo/=10;
            System.out.print(mirror);
        }
        //System.out.println("Сумма ваших чисел = " + sum);

    }
}
