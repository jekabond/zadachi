import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class OperatorsZ5 {
    public void run() throws IOException {
        System.out.println("Написать программу определения оценки студента по его рейтингу");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите ваш рейтинг");
        int rating = Integer.parseInt(reader.readLine());

        if (rating >= 0 && rating < 20)
            System.out.println("Ваша оценка (F)");

        if (rating >= 20 && rating < 39)
            System.out.println("Ваша оценка (E)");

        if (rating >= 40 && rating < 59)
            System.out.println("Ваша оценка (D)");

        if (rating >= 60 && rating < 74)
            System.out.println("Ваша оценка (C)");

        if (rating >= 75 && rating < 89)
            System.out.println("Ваша оценка (B)");

        if (rating >= 90 && rating < 100)
            System.out.println("Ваша оценка (A)");

    }
}
