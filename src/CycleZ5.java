import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class CycleZ5 {
    public void run() throws IOException {
        System.out.println("Посчитать сумму цифр заданного числа");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите число");
        long chislo = Long.parseLong(reader.readLine());
        long sum = 0l;

        while(chislo != 0){
            sum += (chislo % 10);
            chislo/=10;
        }
        System.out.println("Сумма ваших чисел = " + sum);

    }
}
