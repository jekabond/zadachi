import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Controller {

    public Controller() {
    }

    public void init() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Выбирите раздел:");
        System.out.println("1. Условные операторы");
        System.out.println("2. Циклы");
        System.out.println("3. Массивы");
        int razdel = Integer.parseInt(reader.readLine());
        int zadanie = Integer.parseInt(reader.readLine());

        switch (razdel){
            case 1 :
                switch (zadanie){
                    case 1 : new OperatorsZ1().run();
                    break;
                    case 2 : new OperatorsZ2().run();
                    break;
                    case 3 : new OperatorsZ3().run();
                    break;
                    case 4 : new OperatorsZ4().run();
                    break;
                    case 5 : new OperatorsZ5().run();
                    break;
                }
                break;
            case 2 :
                switch (zadanie){
                    case 1 : new CycleZ1().run();
                    break;
                    case 2 : new CycleZ2().run();
                    break;
                    case 3 : new CycleZ3().run();
                    break;
                    case 4 : new CycleZ4().run();
                    break;
                    case 5 : new CycleZ5().run();
                    break;
                    case 6 : new CycleZ6().run();
                    break;
                }
                break;
        }

    }
//    new OperatorsZ1().run();
////                OperatorsZ1 oper1 =new OperatorsZ1();
////                oper1.run();
//                break;

}